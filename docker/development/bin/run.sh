#!/bin/bash
#

# Install dependencies
cd /var/www

apt-get update  -y
apt-get install -y php5-imagick php5-gd php5-intl php5-mcrypt php5-apcu php5-curl php5-mysql acl

# Enable VirtualHost
cp /tmp/docker/development/config/virtual-host.conf /etc/apache2/sites-enabled/0-virtual-host.conf

# Composer Install
wget -q http://getcomposer.org/composer.phar -O ./composer.phar
php composer.phar install

sudo setfacl -R -m u:"www-data":rwX -m u:`whoami`:rwX app/cache app/logs
sudo setfacl -dR -m u:"www-data":rwX -m u:`whoami`:rwX app/cache app/logs

# Symfony2
php app/console doctrine:database:create --env=dev --no-interaction > /dev/null 2>&1
php app/console doctrine:schema:create   --env=dev --no-interaction > /dev/null 2>&1
php app/console doctrine:fixtures:load   --env=dev --no-interaction

# Start Apache
/usr/sbin/apache2 -DFOREGROUND
