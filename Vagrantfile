# -*- mode: ruby -*-
# # vi: set ft=ruby :

require 'yaml'

dir = File.dirname(File.expand_path(__FILE__))

configuration = YAML.load_file("#{dir}/Vagrantfile.yml")

if !configuration['virtual_machine']['provider']['virtualbox'].empty?
    ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'
end

Vagrant.configure("2") do |config|
    config.vm.box = "#{configuration['virtual_machine']['box']['name']}"
    config.vm.box_url = "#{configuration['virtual_machine']['box']['uri']}"

    if configuration['virtual_machine']['hostname'].to_s != ''
        config.vm.hostname = "#{configuration['virtual_machine']['hostname']}"
    end

    if configuration['virtual_machine']['network']['private_network'].to_s != ''
        config.vm.network "private_network", ip: "#{configuration['virtual_machine']['network']['private_network']}"
    end

    if configuration['virtual_machine']['network'].has_key?("forwarded_ports")
        configuration['virtual_machine']['network']['forwarded_ports'].each do |identifier, port|
            if port['guest'] != '' && port['host'] != ''
                config.vm.network :forwarded_port, guest: port['guest'].to_i, host: port['host'].to_i
            end
        end
    end

    # This could probably be cleaner, but my Ruby knowledge sucks
    directory = configuration['virtual_machine']['synced_directory']
    if directory['type'] == "rsync"
        config.vm.synced_folder "#{directory['source']}", "#{directory['target']}", type: "#{directory['type']}", rsync__exclude: directory['exclude']
    elsif directory.has_key?("type")
        config.vm.synced_folder "#{directory['source']}", "#{directory['target']}", type: "#{directory['type']}"
    else
        config.vm.synced_folder "#{directory['source']}", "#{directory['target']}"
    end

    # NFS needs cache and logs in ramdisk
    if directory['type'] != "rsync"
      config.vm.provision :shell, :inline => "mount | grep app/cache; if [[ $? -eq 1 ]]; then mount -t tmpfs -o size=150m,mode=0777 tmpfs /var/www/application/app/cache; fi"
      config.vm.provision :shell, :inline => "mount | grep app/logs;  if [[ $? -eq 1 ]]; then mount -t tmpfs -o size=150m,mode=0777 tmpfs /var/www/application/app/logs; fi"
    end

    if !configuration['virtual_machine']['provider']['virtualbox'].empty?
        config.vm.provider :virtualbox do |virtualbox|
            configuration['virtual_machine']['provider']['virtualbox'].each do |key, value|
                virtualbox.customize ["modifyvm", :id, "--#{key}", "#{value}"]
            end
        end
    end

    config.vm.provision :shell, :inline => "docker inspect blackbird &>/dev/null; if [ $? -eq 0 ]; then docker rm -f blackbird; fi"

    config.vm.provision "docker" do |docker|
        docker.pull_images "mysql"
        docker.pull_images "teamrock/apache2:development"

        docker.run "mysql", image: "mysql",
            args: "--name mysql -e MYSQL_ROOT_PASSWORD=teamrock -p 0.0.0.0:3306:3306"

        docker.run "blackbird", image: "teamrock/apache2:development",
            args: "\
                -v /var/www/application:/var/www \
                -v /var/www/docker:/tmp/docker \
                -v /var/www/logs:/tmp/logs \
                -e 'ENV=development' \
                -p 0.0.0.0:80:80 \
                --privileged \
                --entrypoint /bin/bash",
            cmd:  "/tmp/docker/development/bin/run.sh"
    end
end
