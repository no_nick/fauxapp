<?php
namespace TeamRock\ContentBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsWeightableType extends AbstractType
{
    private $newsWeightableChoices;
    
    public function __construct(array $newsWeightableChoices) {
        $this->newsWeightableChoices = $newsWeightableChoices;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => $this->newsWeightableChoices,
        ));
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'news_weightable';
    }
}