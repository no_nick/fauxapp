<?php

namespace TeamRock\ContentBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * NewsRepository
 */
class NewsRepository extends EntityRepository
{
    public function findSortedByViews($direction = 'desc')
    {
        return $this->findBy(null, [
                'views' => $direction
            ]);
    }
}
