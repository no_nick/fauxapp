<?php

namespace TeamRock\ContentBundle\Traits;

trait DeletableTrait
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean")
     */
    protected $isDeleted = false;
    
    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }
    
    function softDelete()
    {
        $this->setIsDeleted(true);
    }

    function hardDelete()
    {
        $this->softDelete();
    }
}
