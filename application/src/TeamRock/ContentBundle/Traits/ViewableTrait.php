<?php

namespace TeamRock\ContentBundle\Traits;

use TeamRock\ContentBundle\EventDispatcher\FauxEvent; 

trait ViewableTrait
{
    /**
     * @var string
     *
     * @ORM\Column(type="integer")
     */
    protected $views = 0;

    /**
     * @return string
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param string $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    public function onView()
    {
        ++$this->views;
    }
    
    public function onViewEvent(FauxEvent $event) {
        $event->onViewAction($event);
    }
}
