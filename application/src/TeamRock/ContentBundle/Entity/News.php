<?php

namespace TeamRock\ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamRock\ContentBundle\Interfaces\DeletableInterface;
use TeamRock\ContentBundle\Interfaces\ViewableInterface;
use TeamRock\ContentBundle\Traits\DeletableTrait;
use TeamRock\ContentBundle\Traits\ViewableTrait;


/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="TeamRock\ContentBundle\Repository\NewsRepository")
 */
class News implements ViewableInterface, DeletableInterface
{
    /**
     * @var guid
     * @ORM\Column(name="identifier", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $identifier;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="news_weight", type="integer", length=45, nullable=true)
     */
    protected $newsWeight;
    
    /*
     * Traits
     */
    use ViewableTrait;
    use DeletableTrait;

    /**
     * Get id
     *
     * @return integer
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set newsWeight
     *
     * @param integer $newsWeight
     * @return News
     */
    public function setNewsWeight($newsWeight)
    {
        $this->newsWeight = $newsWeight;

        return $this;
    }

    /**
     * Get newsWeight
     *
     * @return integer 
     */
    public function getNewsWeight()
    {
        return $this->newsWeight;
    }
}
