<?php

namespace TeamRock\ContentBundle\Twig;

class CustomFormChoicesExtension extends \Twig_Extension
{
    private $newsWeightable;
    
    public function __construct(array $newsWeightableChoices) {
        $this->newsWeightable = $newsWeightableChoices;
    }
    
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('newsWeight', array($this, 'newsWeightFilter')),
        );
    }

    public function newsWeightFilter($key)
    {
        return $this->newsWeightable[$key];
    }
    
    public function getName()
    {
        return 'custom_form_choices_extension';
    }
}