<?php

namespace TeamRock\ContentBundle\EventDispatcher;

use Symfony\Component\EventDispatcher\Event;
use TeamRock\ContentBundle\Entity\News;

class FauxEvent extends Event
{
    private $news;
    private $em;
    
    public function __construct(News $news, $em) {
        $this->news = $news;
        $this->em = $em;
    }

    public function getNews()
    {
        return $this->news;
    }
    
    public function onViewAction(Event $event)
    {
        $views = $event->getNews()->getViews();
        $event->getNews()->setViews(++$views);
        $em = $this->em;
        $em->flush();
    }
}