<?php

namespace TeamRock\FauxBundle\Controller;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;
use TeamRock\ContentBundle\EventDispatcher\FauxEvent;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use TeamRock\ContentBundle\Entity\News;
use TeamRock\ContentBundle\Form\NewsAddType;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        return [ ];
    }
    
    /**
     * @Route("/list/trash", name="news_list_deleted")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function listNewsMarkedAsDeletedAction(Request $request)
    {
        $newsRepository = $this->getDoctrine()->getRepository("TeamRockContentBundle:News");
        $news = $newsRepository->findBy(array('isDeleted' => true), array('title' => 'asc'));
        
        // We could place here usual link but instead
        // I created delete forms for each news to create unique tokens
        // (prevent deletion via browser url, /delete/667264bd-989e-11e4-b509-20cf300651f6)
        $deleteForms = array();
        foreach ($news as $entity) {
            $deleteForms[$entity->getIdentifier()] = $this->createDeleteForm($entity->getIdentifier())->createView();
        }
        
        return [ 'news' => $news, 'deleteForms' => $deleteForms ];
    }
    
    /**
     * @Route("/list/{order}", name="news_list_order", defaults={"order" = "asc"})
     * @Route("/list", name="news_list")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function listNewsAction(Request $request, $order = NULL)
    {
        $newsRepository = $this->getDoctrine()->getRepository("TeamRockContentBundle:News");
        switch(mb_strtolower($order)){
            case 'asc': 
                $news = $newsRepository->findBy(array('isDeleted' => false), array('views' => 'asc', 'title' => 'asc'));
                break;
            case 'desc': 
                $news = $newsRepository->findBy(array('isDeleted' => false), array('views' => 'desc', 'title' => 'asc'));
                break;
            default: 
                $news = $newsRepository->findBy(array('isDeleted' => false), array('title' => 'asc')); 
                break;
        }

        // We could place here usual link but instead
        // I created delete forms for each news to create unique tokens
        // (prevent deletion via browser url, /delete/667264bd-989e-11e4-b509-20cf300651f6)
        $deleteForms = array();
        foreach ($news as $entity) {
            $deleteForms[$entity->getIdentifier()] = $this->createDeleteForm($entity->getIdentifier())->createView();
        }
        
        return [ 'news' => $news, 'order' => $order, 'deleteForms' => $deleteForms ];
    }

    /**
     * @Route("/add", name="news_add")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function addNewsAction(Request $request)
    {
        $news = new News();
        $newsType = new NewsAddType();

        $newsForm = $this->createForm($newsType, $news);
        $newsForm->handleRequest($request);

        if ($newsForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($news);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('news_list'));
        }

        return [ 'newsForm' => $newsForm->createView() ];
    }

    /**
     * @Route("/view/{identifier})", name="news_view")
     * @Template()
     */
    public function viewNewsAction($identifier)
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository("TeamRockContentBundle:News")->find($identifier);

        if (!$news) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }
        
        $event = new FauxEvent($news, $em);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch('teamrock.events.foo', $event);
        
        return [ 'news' => $news ];
    }
    
    /**
     * @Route("/markDeleted/{identifier})", name="news_mark_as_deleted")
     */
    public function markAsDeleted($identifier)
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository("TeamRockContentBundle:News")->find($identifier);
        
        if (!$news) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }
        
        $news->softDelete();
        $em->flush();
        
        return $this->redirect($this->getRequest()->headers->get('referer'));
    }
    
    /**
     * @Route("/restore/{identifier})", name="news_restore")
     */
    public function newsRestore($identifier)
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository("TeamRockContentBundle:News")->find($identifier);
        
        if (!$news) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }
        
        $news->setIsDeleted(false);
        $em->flush();
        
        return $this->redirect($this->getRequest()->headers->get('referer'));
    }
    
    /**
     * Deletes a News entity.
     *
     * @Route("/delete/{identifier}", name="news_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $identifier)
    {
        $form = $this->createDeleteForm($identifier);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TeamRockContentBundle:News')->find($identifier);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find News entity.');
            }
            
            $entity->softDelete();
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

    /**
     * Creates a form to delete a City entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($identifier)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('news_delete', array('identifier' => $identifier)))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
