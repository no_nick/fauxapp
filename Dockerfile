#
FROM teamrock/apache2:production

ADD ./docker/production/config/virtual-host.conf /etc/apache2/sites-enabled/0-virtual-host.conf

RUN apt-get install -y php5-imagick php5-curl

ADD ./docker /tmp/docker
ADD ./application /var/www

WORKDIR /var/www

RUN wget http://getcomposer.org/composer.phar
RUN php composer.phar install --optimize-autoloader --prefer-dist

RUN chown -R www-data:www-data /var/www
